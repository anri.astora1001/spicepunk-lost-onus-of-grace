function DialogueBox(props) {
  return (
    <div className={`$ mt-3 text-box p-3 overflow-auto ${props.className}`}>{props.children}</div>
  );
}

function DialogueEntry(props) {
  const speaker = props?.speaker;

  return (
    <div className="row g-0 text-accent">
      <div className=" w-10 border-end">{speaker}</div>
      <div className="col ps-3">{props.children}</div>
    </div>
  );
}

function QuestEntry(props) {
  const quest = props?.quest;
  return (
    <div className="row g-0 text-accent">
      <div className="w-15 border-end">{quest.name}</div>
      <div className="col ps-3">
        <div className="">Progress: {quest.progress}</div>
        <div className="">{quest.description}</div>
      </div>
    </div>
  );
}

export { DialogueBox, DialogueEntry, QuestEntry };
