import MainButton from "../common/MainButton";
import { DialogueBox, DialogueEntry } from "./DialogeComps";
import { useState, useEffect, useCallback } from "react";
import {
  getGreeting,
  getPlayerGreeting,
} from "../common/Dialogue/Dialogue-util";
import LargeModal from "../common/Modals/LargeModal";
import RumorModalBody from "./ModalBodies/RumorModalBody";
import JokeModalBody from "./ModalBodies/JokeModalBody";


export default function DialoguePage(props) {
  const npcData = {
    name: "Boobah Time",
  };

  const [dc, setDC] = useState(1);
  const [greeted, setGreeted] = useState(false);
  const [showInspect, setShowInspect] = useState(false);

  const [dialogue, setDialogue] = useState([
    <DialogueEntry key="d0" speaker={"NPC"}>
      {getGreeting()}
    </DialogueEntry>,
  ]);
  const [test, setTest] = useState(false);

  const handleInspect = () => {
    setShowInspect(!showInspect);
  };

  const handleGreet = () => {
    if (!greeted) {
      const greeting = getPlayerGreeting();
      const currentDia = dialogue;
      currentDia.push(
        <DialogueEntry key={`d${dc}`} speaker={"You"}>
          {greeting}
        </DialogueEntry>
      );
      setDialogue(currentDia);
      setGreeted(true);
      setDC(dc + 1);
      setTest(!test);
    }
  };

  const [toggleTalk, setToggleTalk] = useState(false);

  const handleTalk = () => {
    setToggleTalk(!toggleTalk);
    setTest(!test);
  };

  return (
    <div className="container-fluid vh-100 bgc-dark">
      {test && <div></div>}

      <div className="row h-75 ">
        <div className="col bgc-dark2">
          <DialogueBox>{dialogue}</DialogueBox>
        </div>
      </div>
      <div className="row h-8 bgc-dark3">
        {!greeted && [
          <MainButton key="t1">
            <div className="bgc-accent btn-c" onClick={handleGreet}>
              <span className="mx-auto">Greet</span>
            </div>
          </MainButton>,
          <MainButton key="t2">
            <div className="bgc-accent btn-c" onClick={handleTalk}>
              <span className="mx-auto">Talk</span>
            </div>
          </MainButton>,
          <MainButton key="t3">
            <div className="bgc-accent btn-c" onClick={handleInspect}>
              <span className="mx-auto">Inspect</span>
            </div>
          </MainButton>,
        ]}
        {greeted && [
          <MainButton key="t1">
            <div className="bgc-accent btn-c" onClick={handleTalk}>
              <span className="mx-auto">Talk</span>
            </div>
          </MainButton>,
          <MainButton key="t2">
            <div className="bgc-accent btn-c" onClick={handleInspect}>
              <span className="mx-auto">Inspect</span>
            </div>
          </MainButton>,
          <MainButton key="t3" />,
        ]}
        <MainButton />
        <MainButton />
        <MainButton />
      </div>
      {!toggleTalk && (
        <div className="row h-8 bgc-dark3">
          <MainButton key="m1" />
          <MainButton key="m2" />
          <MainButton key="m3" />
          <MainButton key="m4" />
          <MainButton key="m5" />
          <MainButton key="m6" />
        </div>
      )}
      {toggleTalk && (
        <div className="row h-8 bgc-dark3">
          <DiaButton
            type={["rumor", "Rumors"]}
            dialogue={dialogue}
            changeDialogue={setDialogue}
            test={test}
            changeTest={setTest}
          />
          <DiaButton
            type={["joke", "Joke"]}
            dialogue={dialogue}
            changeDialogue={setDialogue}
            test={test}
            changeTest={setTest}
          />
          <DiaButton
            type={["flirt", "Flirt"]}
            dialogue={dialogue}
            changeDialogue={setDialogue}
            test={test}
            changeTest={setTest}
          />
          <MainButton key="m4" />
          <MainButton key="m5" />
          <MainButton key="m6" />
        </div>
      )}

      <div className="row h-8 bgc-dark3">
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
      </div>
      <LargeModal
        data={npcData}
        type={["npc-inspect", npcData.name]}
        display={showInspect}
        displayToggle={setShowInspect}
      />
    </div>
  );
}

function DiaButton(props) {
  const type = props.type;
  const [toggle, setToggle] = useState(false);

  const toggleModal = () => {
    setToggle(!toggle);
  };

  return [
    <MainButton key="m1">
      <div className="bgc-accent btn-c" onClick={toggleModal}>
        <span className="mx-auto">{type[1]}</span>
      </div>
    </MainButton>,
    <DiaModal
      key={`modal:${type[1]}`}
      type={type}
      display={toggle}
      displayToggle={setToggle}
      dialogue={props.dialogue}
      changeDialogue={props.changeDialogue}
      test={props.test}
      changeTest={props.changeTest}
    />,
  ];
}

function DiaModal(props) {
  const [title, setTitle] = useState(props.type[1]);
  const [type, setType] = useState(props.type);

  const [rumor, setRumor] = useState(false);
  const [joke, setJoke] = useState(false);
  const [flirt, setFlirt] = useState(false);

  const getComponents = () => {
    if (type[0] === "rumor") {
      setTitle(type[1]);
      setRumor(true);
      setJoke(false);
    } else if (type[0] === "joke") {
      setTitle(type[1]);
      setJoke(true);
      setRumor(false);
    } else if (type[0] === "flirt") {
    }
  };

  useEffect(() => {
    getComponents();
  }, [type]);

  const closeButton = () => {
    setTitle(props.type[1]);
    setType(props.type);
    props.displayToggle(false);
  };

  if (!props.display) return;
  return (
    <div className="dia-modal">
      <div className="row w-100 text-center mt-2 g-0 border-bottom-c-accent pb-1">
        <h4 className="col text-accent"> {title} </h4>
        <div
          className="d-flex justify-content-end pe-2 pt-1"
          style={{ width: "50px" }}
        >
          <div className="btn-close  bgc-accent" onClick={closeButton}></div>
        </div>
      </div>
      <div className="row g-0  px-3 pt-2">
        {rumor && (
          <RumorModalBody
            changeType={setType}
            type={type}
            dialogue={props.dialogue}
            changeDialogue={props.changeDialogue}
            test={props.test}
            changeTest={props.changeTest}
          />
        )}
        {joke && (
          <JokeModalBody
            changeType={setType}
            type={type}
            dialogue={props.dialogue}
            changeDialogue={props.changeDialogue}
            test={props.test}
            changeTest={props.changeTest}
          />
        )}
      </div>
    </div>
  );
}
