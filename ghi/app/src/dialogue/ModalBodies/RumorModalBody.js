import MainButton from "../../common/MainButton";
import { randArr } from "../../common/util";
import { DialogueEntry } from "../DialogeComps";
import { useState } from "react";

export default function RumorModalBody(props) {
  const [i, setI] = useState(0);
  // console.log(props)
  const handleRumors = () => {
    console.log(props.dialogue);
    const rumor = getRumor();
    const temp = props.dialogue;
    temp.push(
      <DialogueEntry key={`rumor:${i}`} speaker={"You"}>
        heard any rumors lately?
      </DialogueEntry>
    );
    temp.push(
      <DialogueEntry key={`rumor:${i + 1}`} speaker={"NPC"}>
        {rumor}
      </DialogueEntry>
    );
    props.changeDialogue(temp);
    props.changeTest(!props.test);
    setI(i + 2)
  };


  return (
    <div className="vh-100 bgc-dark">
      <div className="row h-23 bgc-dark3">
        <MainButton>
          <div className="bgc-accent btn-c" onClick={handleRumors}>
            <span className="mx-auto">Local Rumors</span>
          </div>
        </MainButton>
        <MainButton>
          <div className="bgc-accent btn-c">
            <span className="mx-auto">Jobs</span>
          </div>
        </MainButton>
      </div>
    </div>
  );
}

function getRumor() {
  const rumors = ["rumor 1", "rumor 2", "rumor 3"];
  return rumors[randArr(rumors.length)];
}
