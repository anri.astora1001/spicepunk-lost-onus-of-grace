import { useState } from "react";
import MainButton from "../common/MainButton";
import InvAll from "./InvAll";
import InvStatus from "./InvStatus";
import InvEquipment from "./InvEquipment";

export default function InventoryPage() {
  const items = [
    {
      name: "Sword",
      weight: 2,
      value: 50,
      fav: true,
      scaling: {
        prowess: "C",
        cantillation: "",
        understanding: "",
        licentiousness: "",
        insight: "",
        agility: "C",
      },
    },
    {
      name: "Bow",
      weight: 1,
      value: 25,
      scaling: {
        prowess: "",
        cantillation: "",
        understanding: "",
        licentiousness: "",
        insight: "",
        agility: "B",
      },
    },
    { name: "Arrow", weight: 0.1, value: 1, amount: 30 },
  ];

  const [focus, setFocus] = useState("all");

  const handleFocusChange = (e) => {
    const val = e.currentTarget.value
    setFocus(val)
  }

  // console.log(focus)

  return (
    <div className="container-fluid vh-100 bgc-dark">
      <div className="row bgc-dark2 h-5">
        <MainButton>
          <button className="bgc-accent btn-c" value="status" onClick={handleFocusChange}>
            <span className="mx-auto">Status</span>
          </button>
        </MainButton>
        <MainButton>
          <button className="bgc-accent btn-c" value="all" onClick={handleFocusChange}>
            <span className="mx-auto">All</span>
          </button>
        </MainButton>
        <MainButton>
          <button className="bgc-accent btn-c" value="equipment" onClick={handleFocusChange}>
            <span className="mx-auto">Equipment</span>
          </button>
        </MainButton>
        <MainButton>
          <button className="bgc-accent btn-c" value="aid" onClick={handleFocusChange}>
            <span className="mx-auto">Aid</span>
          </button>
        </MainButton>
        <MainButton>
          <button className="bgc-accent btn-c" value="ingredients" onClick={handleFocusChange}>
            <span className="mx-auto">Ingredients</span>
          </button>
        </MainButton>
        <MainButton>
          <button className="bgc-accent btn-c" value="misc" onClick={handleFocusChange}>
            <span className="mx-auto">Misc</span>
          </button>
        </MainButton>
      </div>
      {focus === "status" && <InvStatus />}
      {focus === "all" && <InvAll items={items} />}
      {focus === "equipment" && <InvEquipment />}
      {focus === "aid"}
      {focus === "ingredients"}
      {focus === "misc"}
    </div>
  );
}
