import { useState } from "react";
import ScalingTable from "./ScalingTable";

export default function InvAll(props){
    const items = props.items

    const [focusItem, setFocusItem] = useState(items[0]);

    return (
        <div className="row  h-95 ">
        <div className="bgc-dark3 w-25 rel p-0">
          <div className="row">
            <div className="btn-c w-25">Icon</div>
          </div>
          <h1 className="text-accent text-center">{focusItem.name}</h1>
          <div className="text-accent px-3">
            Sample Text that describes the focused item
          </div>
          <div className="abs w-100" style={{ bottom: "0%" }}>
            <ScalingTable item={focusItem} />
          </div>
        </div>
        <div className="w-75 h-100">
          <div className="h-5 d-flex flex-row-reverse text-accent align-items-center">
            <div className="justify-self-end">
              Load: {getTotalWeight(items)}/100
            </div>
          </div>
          <table className="table table-striped table-dark text-accent ">
            <thead>
              <tr>
                <th>Name</th>
                <th>Weight</th>
                <th>Value</th>
                <th>V/W</th>
              </tr>
            </thead>
            <tbody>
              {items.map((item, i) => {
                return <ItemEntry key={`all:${i}`} item={item} setFocus={setFocusItem} />;
              })}
            </tbody>
          </table>
        </div>
      </div>
    )
}

function getTotalWeight(items) {
    let sum = 0;
    items.forEach((item) => {
      if (item.amount) {
        sum += item.weight * item.amount;
      } else {
        sum += item.weight;
      }
    });
    return sum;
  }

function ItemEntry(props) {
    const item = props.item;
    const vw = item.value / item.weight;
  
    const handleHover = () => {
      props.setFocus(item);
    };
    return (
      <tr onMouseEnter={handleHover}>
        <td className="d-flex align-items-center">
          {item.name}
          {item.fav && (
            <svg className="ms-1" height="20" width="20">
              <polygon
                points="10,1 3,20 20,8 0,8 17,20"
                style={{
                  fill: "gold",
                  stroke: "5",
                  strokeWidth: "1",
                  fillRule: "nonzero",
                }}
              />
            </svg>
          )}
        </td>
        <td>{item.weight}</td>
        <td>{item.value}</td>
        <td>{vw}</td>
      </tr>
    );
  }