import GameText from "../text/GameText";

export default function InvStatus(props) {
  const stats = {
    level: 10,
    corruption: 0,

    prowess: 50,
    effusion: 10,
    cantillation: 10,
    understanding: 10,
    licentiousness: 10,
    insight: 10,
    agility: 10,
    resilience: 15,
  };

  const status = {
    hp: 1000,
    thirst: 0.5,
    hunger: 0.25,
  };

  return (
    <div className="row  h-95 ">
      <div className="bgc-dark3 w-25 p-0">
        <h1 className="text-accent text-center">Status Effects</h1>
      </div>
      <div className="w-75 h-75 p-0">
        <div className="row g-0 border w-100 h-100">
          <div className="col h-100">
            <div className="row g-0">
              <h3 className="w-25 text-accent"> Aura: </h3>
              <span
                className="col d-flex align-items-center justify-content-center text-white"
                style={{ backgroundColor: "white",  boxShadow: `inset 0 0 999px ${calcAura(stats)}` }}
              >White</span>
              <span
                className="col d-flex align-items-center justify-content-center text-black"
                style={{ backgroundColor: "black", boxShadow: `inset 0 0 999px ${calcAura(stats)}` }}
              >Black</span>
            </div>
            <div className="row g-0 text-accent">
              <h3 className="w-25"> Thirst: </h3>
              <span
                className="d-flex align-items-center justify-content-center"
                style={{
                  backgroundColor: "blue",
                  width: `${75 * status.thirst}%`,
                  minWidth: "0%",
                }}
              >{`${status.thirst * 100}%`}</span>
            </div>
            <div className="row g-0 text-accent">
              <h3 className="w-25"> Hunger: </h3>
              <span
                className="d-flex align-items-center justify-content-center"
                style={{
                  backgroundColor: "orange",
                  width: `${75 * status.hunger}%`,
                  minWidth: "0%",
                }}
              >{`${status.hunger * 100}%`}</span>
            </div>
          </div>
          <BodyStatus />
          <div className="col border h-100 text-accent">sample text</div>
        </div>
        <div className="bgc-dark2 w-100 h-33">sample text</div>
      </div>
    </div>
  );
}

function BodyStatus() {
  const body = {
    head: {
      health: 1,
    },
    torso: {
      health: 0.5,
    },
    leftArm: {
      health: 0.1,
      crippled: true,
    },
    rightArm: {
      health: 0.25,
    },
    leftLeg: {
      health: 0.75,
    },
    rightLeg: {
      health: 0.9,
    },
  };

  return (
    <div className="col border h-100 text-accent">
      <div
        className="w-25 mx-auto h-15 mt-3"
        // style={{ boxShadow: `inset 0 0 999px ${calcRgba(body.head.health)}${calcRgba(body.head.health)}` }}
        style={{
          backgroundColor: "red",
          filter: `hue-rotate(${calcShift(body.head.health)}deg)`,
        }}
      >
        Head Icon
      </div>
      <div className="row g-0 h-33 d-flex justify-content-center mt-3">
        <div
          className=" w-25"
          style={{
            backgroundColor: "red",
            filter: `hue-rotate(${calcShift(body.rightArm.health)}deg)`,
          }}
        >
          arm right
        </div>
        <div
          className="w-33 mx-3"
          style={{
            backgroundColor: "red",
            filter: `hue-rotate(${calcShift(body.torso.health)}deg)`,
          }}
        >
          torso
        </div>
        <div
          className="w-25"
          style={{
            backgroundColor: "red",
            filter: `hue-rotate(${calcShift(body.leftArm.health)}deg)`,
          }}
        >
          arm left
        </div>
      </div>
      <div className="row g-0 h-40 d-flex justify-content-center  mt-3">
        <div
          className="w-25"
          style={{
            backgroundColor: "red",
            filter: `hue-rotate(${calcShift(body.leftLeg.health)}deg)`,
          }}
        >
          {" "}
          leg right
        </div>
        <div className="w-0 mx-2"></div>
        <div
          className="w-25"
          style={{
            backgroundColor: "red",
            filter: `hue-rotate(${calcShift(body.rightLeg.health)}deg)`,
          }}
        >
          {" "}
          leg left
        </div>
      </div>
    </div>
  );
}

function calcShift(health) {
  const total = 120;
  const toGreen = total * health;

  return toGreen;
}

function calcAura(stats) {
  let red = 50;
  let green = 50;
  let blue = 50;
  let opacity = 0.25;

  const offset = 8;
  const mult = 2.5;

  //levels
  opacity += 0.01 * stats.level;

  //prowess
  red += (stats.prowess - offset) * mult;

  //effusion
  red += (stats.effusion - offset) * mult * 0.3;
  green += (stats.effusion - offset) * mult * 0.3;
  blue += (stats.effusion - offset) * mult * 0.3;

  //cantillation
  red += (stats.cantillation - offset) * mult * 0.5;
  green += (stats.cantillation - offset) * mult * 0.5;

  //understanding
  blue += (stats.understanding - offset) * mult;

  //licentiousness
  green -= (stats.licentiousness - offset) * mult;

  //insight
  green += (stats.insight - offset) * mult * 0.5;
  blue += (stats.insight - offset) * mult * 0.5;

  //agility
  green += (stats.agility - offset) * mult;

  //resilience
  red -= (stats.resilience - offset) * mult * 0.5;
  blue -= (stats.resilience - offset) * mult * 0.5;

  //corruption
  red -= stats.corruption * mult * 0.5;
  blue -= stats.corruption * mult * 0.5;
  green -= stats.corruption * mult * 0.5;
  console.log(
    "red: ",
    red,
    "green: ",
    green,
    "blue: ",
    blue,
    "opacity: ",
    opacity
  );

  return `rgba(${red},${green},${blue},${opacity})`;
}
