import { useState } from "react";

export default function EquippedTable(props) {
  const items = props.items;
  const player = props.player;

  const [armor, setArmor] = useState(true);
  const [talismans, setTalismans] = useState(true);
  const [clothing, setClothing] = useState(true);
  const [equipSlots, setEquipSlots] = useState(true);
  const [beltToggle, setBeltToggle] = useState(true);
  const [backToggle, setBackToggle] = useState(true);

  const upArrow = (
    <svg viewBox="0 0 24 24" width="20" height="20" fill="white">
      <path d="M18.78 15.78a.749.749 0 0 1-1.06 0L12 10.061 6.28 15.78a.749.749 0 1 1-1.06-1.06l6.25-6.25a.749.749 0 0 1 1.06 0l6.25 6.25a.749.749 0 0 1 0 1.06Z"></path>
    </svg>
  );

  const downArrow = (
    <svg viewBox="0 0 24 24" width="20" height="20" fill="white">
      <path d="M5.22 8.22a.749.749 0 0 0 0 1.06l6.25 6.25a.749.749 0 0 0 1.06 0l6.25-6.25a.749.749 0 1 0-1.06-1.06L12 13.939 6.28 8.22a.749.749 0 0 0-1.06 0Z"></path>
    </svg>
  );

  //Slots
  const belt = items.find((item) => {
    return item.equipped === "belt";
  });
  const back = items.find((item) => {
    return item.equipped === "back";
  });

  //Armor
  const head = items.find((item) => {
    return item.equipped === "helmet";
  });

  const torso = items.find((item) => {
    return item.equipped === "torso";
  });

  const rightArm = items.find((item) => {
    return item.equipped === "rightArm";
  });

  const leftArm = items.find((item) => {
    return item.equipped === "leftArm";
  });

  const legs = items.find((item) => {
    return item.equipped === "legs";
  });

  const feet = items.find((item) => {
    return item.equipped === "feet";
  });

  //Talismans
  const talisma = items.filter((item) => {
    return item.equipped === "talisman";
  });

  //Clothing
  const hat = items.find((item) => {
    return item.equipped === "hat";
  });

  const spectacles = items.find((item) => {
    return item.equipped === "spectacles";
  });

  const face = items.find((item) => {
    return item.equipped === "face";
  });

  const neck = items.find((item) => {
    return item.equipped === "neck";
  });

  const cape = items.find((item) => {
    return item.equipped === "cape";
  });

  const jacket = items.find((item) => {
    return item.equipped === "jacket";
  });

  const shirt = items.find((item) => {
    return item.equipped === "shirt";
  });

  const pants = items.find((item) => {
    return item.equipped === "pants";
  });

  const socks = items.find((item) => {
    return item.equipped === "socks";
  });

  const armorToggle = () => {
    setArmor(!armor);
    setClothing(!clothing);
  };

  const talismansToggle = () => {
    setTalismans(!talismans);
  };

  const clothingToggle = () => {
    setClothing(!clothing);
    setArmor(!armor);
  };

  const equipSlotsToggle = () => {
    setEquipSlots(!equipSlots);
  };

  const handleBeltToggle = () => {
    setBeltToggle(!beltToggle);
  };

  const handleBackToggle = () => {
    setBackToggle(!backToggle);
  };

  //   console.log(belt?.slots)
  //   console.log(belt)

  return (
    <div className="overflow-auto">
      <table className="table table-striped table-dark text-accent">
        <thead onClick={equipSlotsToggle}>
          <tr>
            <th>EquipSlots</th>
            <th className="text-end">{equipSlots ? downArrow : upArrow}</th>
          </tr>
        </thead>
        {equipSlots && (
          <tbody>
            <tr onClick={handleBackToggle}>
              <td>Back:</td>
              <td>
                <span className="row w-100 g-0">
                  <span className="d-flex w-50">{back?.name}</span>
                  <span className="d-flex justify-content-end w-50">
                    {backToggle ? downArrow : upArrow}
                  </span>
                </span>
              </td>
            </tr>
            {back?.slots &&
              backToggle &&
              back.slots.map((slot, i) => {
                if (slot.equipped)
                  return (
                    <EquipRow
                      className={"ps-5"}
                      key={`${i}:beltSlot`}
                      item={slot.equipped}
                    >
                      {slot.type}
                    </EquipRow>
                  );
                return (
                  <EquipRow className={"ps-5"} key={`${i}:backSlot`}>
                    {slot.type}
                  </EquipRow>
                );
              })}
            <tr onClick={handleBeltToggle}>
              <td>Belt:</td>
              <td>
                <span className="row w-100 g-0">
                  <span className="d-flex w-50">{belt?.name}</span>
                  <span className="d-flex justify-content-end w-50">
                    {beltToggle ? downArrow : upArrow}
                  </span>
                </span>
              </td>
            </tr>
            {belt?.slots &&
              beltToggle &&
              belt.slots.map((slot, i) => {
                if (slot.equipped)
                  return (
                    <EquipRow
                      className={"ps-5"}
                      key={`${i}:beltSlot`}
                      item={slot.equipped}
                    >
                      {slot.type}
                    </EquipRow>
                  );
                return (
                  <EquipRow className={"ps-5"} key={`${i}:beltSlot`}>
                    {slot.type}
                  </EquipRow>
                );
              })}
          </tbody>
        )}
      </table>
      <div className="row g-0">
      <table className="col table table-striped table-dark text-accent">
        <thead onClick={talismansToggle}>
          <tr>
            <th>Talismans</th>
            <th className="text-end"></th>
          </tr>
        </thead>
        <tbody>
          {talismans &&
            player.talismanSlots.map((talisman, i) => {
              talisman = talisma[i];
              return (
                <EquipRow key={`talisma:${i}`} item={talisman}>
                  Talisman {`${i + 1}`}:
                </EquipRow>
              );
            })}
        </tbody>
      </table>

      <table className="col table table-striped table-dark text-accent  ">
        <thead onClick={talismansToggle} className="">
          <tr className="h-5 w-100">
            <th className="">Accessories</th>
            <th className="text-end">{talismans ? downArrow : upArrow}</th>
          </tr>
        </thead>
        {talismans && (
          <tbody>
            <EquipRow item={spectacles}>Spectacles:</EquipRow>
            <EquipRow item={face}>Face:</EquipRow>
            <EquipRow item={neck}>Neck:</EquipRow>
            <EquipRow item={cape}>Cape:</EquipRow>
          </tbody>
        )}
      </table>
      </div>

      <div className="row g-0 ">
        <table className="col table table-striped table-dark text-accent">
          <thead onClick={armorToggle}>
            <tr className="">
              <th>Armor</th>
              <th className=""></th>
            </tr>
          </thead>
          {armor && (
            <tbody>
              <EquipRow item={head}>Helmet:</EquipRow>
              <EquipRow item={torso}>Torso:</EquipRow>
              <EquipRow item={rightArm}>RightArm:</EquipRow>
              <EquipRow item={leftArm}>LeftArm:</EquipRow>
              <EquipRow item={legs}>Legs:</EquipRow>
              <EquipRow item={feet}>Feet:</EquipRow>
            </tbody>
          )}
        </table>

        <table className="col table table-striped table-dark text-accent  ">
          <thead onClick={clothingToggle} className="">
            <tr className="h-5 w-100">
              <th className="">Clothing</th>
              <th className="text-end">{clothing ? downArrow : upArrow}</th>
            </tr>
          </thead>
          {clothing && (
            <tbody>
              <EquipRow item={hat}>Hat:</EquipRow>
              <EquipRow item={jacket}>Jacket:</EquipRow>
              <EquipRow item={shirt}>Shirt:</EquipRow>
              <EquipRow item={hat}>Gloves:</EquipRow>
              <EquipRow item={pants}>Pants:</EquipRow>
              <EquipRow item={socks}>Socks:</EquipRow>
            </tbody>
          )}
        </table>
      </div>
    </div>
  );
}

function EquipRow(props) {
  const item = props?.item;

  return (
    <tr>
      <td className={props.className}>{props.children}</td>
      <td>{item?.name}</td>
    </tr>
  );
}
