import { useState, useEffect } from "react";
import EquippedTable from "./EquippedTable";

import { cycleArr } from "../common/util";

export default function InvEquipment() {
  const [player, setPlayer] = useState({
    talismanSlots: [
      { name: "talisman1" },
      { name: "talisman2" },
      { name: "talisman3" },
      { name: "talisman4" },
    ],
  });
  const [items, setItems] = useState([
    {
      id: "testF",
      name: "Transforming Sword",
      state: "A",
      states: ["A","B"],
      A:{name:"sword-mode"},
      B:{name:"spear-mode"},
      weight: 3,
      value: 50,
      damage: "1d8",
      fav: true,
      slotType: "medium",
      type: ["weapon", "versatile"],
    },
    {
      id: "testE",
      name: "bow",
      weight: 2,
      value: 25,
      damage: "1d6",
      slotType: "large",
      type: ["weapon", "ranged", "two-handed"],
    },
    {
      id: "testFF",
      name: "Firecracker",
      weight: 2,
      value: 25,
      damage: "1d6",
      amount: 10,
      slotType: "pouch",
      type: ["weapon", "ranged", "thrown", "consumable"],
    },
    {
      id: "testD",
      name: "arrow",
      weight: 0.01,
      value: 1,
      damage: "+2",
      amount: 10,
      slotType: "quiver",
      type: ["ammo", "arrow"],
    },
    { name: "pants", weight: 1, value: 10, type: ["clothing", "pants"] },
    {
      id: "testC",
      name: "helmet",
      weight: 2,
      value: 15,
      equipped: "helmet",
      type: ["armor", "helmet"],
      absorption: {
        physical: 0.2,
        magic: 0.3,
        thrust: 0.1,
        slash: 0.15,
        crush: 0.2,
      },
    },
    {
      id: "testB",
      name: "helmet 2",
      weight: 2,
      value: 15,
      type: ["armor", "helmet"],
      absorption: {
        physical: 0.2,
        magic: 0.3,
        thrust: 0.1,
        slash: 0.15,
        crush: 0.2,
      },
    },
    {
      id: "testBB",
      name: "Talisman 1",
      weight: 2,
      value: 15,
      type: ["talisman", "talisman 1"],
    },
    {
      id: "testBBB",
      name: "Talisman 2",
      weight: 2,
      value: 15,
      type: ["talisman", "talisman 2"],
    },
    {
      id: "testBBX",
      name: "Talisman 3",
      weight: 2,
      value: 15,
      type: ["talisman", "talisman 3"],
    },
    {
      id: "testBBx",
      name: "Talisman 4",
      weight: 2,
      value: 15,
      type: ["talisman", "talisman 4"],
    },
    {
      id: "testBBxx",
      name: "Talisman 5",
      weight: 2,
      value: 15,
      type: ["talisman", "talisman 5"],
    },
    {
      id: "testA",
      name: "Belt 1",
      weight: 2,
      value: 15,
      type: ["slot", "belt"],
      slots: [
        { type: "medium" },
        { type: "medium" },
        { type: "pouch" },
        { type: "quiver" },
      ],
      absorption: {
        physical: 0.02,
        magic: 0.01,
        thrust: 0.01,
        slash: 0.05,
        crush: 0.02,
      },
    },
    {
      id: "testAA",
      name: "Backpack",
      weight: 2,
      value: 15,
      contains: 50,
      volume: 1,
      type: ["slot", "back"],
      slots: [
        { type: "large" },
        { type: "large" },
        { type: "medium" },
        { type: "quiver" },
      ],
      absorption: {
        physical: 0.02,
        magic: 0.01,
        thrust: 0.01,
        slash: 0.05,
        crush: 0.02,
      },
    },
  ]);

  const [update, setUpdate] = useState(true);

  const [activate, setActivate] = useState("e");
  const [trickKey, setTrickKey] = useState("r")

  useEffect(() => {
    navigator?.keyboard?.getLayoutMap().then((map) => {
      setActivate(map.get("KeyE"));
      setTrickKey(map.get("KeyR"));
    });
  }, []);

  //console.log(items);

  return (
    <div className="row  h-95 overflow-auto">
      {update && <div></div>}
      <div className="bgc-dark3 h-100 w-33 p-0" style={{ overflowY: "auto" }}>
        <h3 className="text-accent text-center">Equipment</h3>
        <EquippedTable items={items} player={player} />
      </div>
      <div className="w-66 h-95 p-0 rel">
        <table className="table table-striped table-dark text-accent ">
          <thead>
            <tr>
              <th>Name</th>
              <th>Damage</th>
              <th>Weight</th>
              <th>Value</th>
              <th>V/W</th>
            </tr>
          </thead>
          <tbody>
            {items.map((item, i) => {
              return (
                <ItemEntry
                  key={`all:${i}`}
                  item={item}
                  activate={activate}
                  trickKey={trickKey}
                  itemIndex={i}
                  items={items}
                  setItems={setItems}
                  update={update}
                  setUpdate={setUpdate}
                  player={player}
                />
              );
            })}
          </tbody>
        </table>
        <ButtonHelpBar activate={activate} trickKey={trickKey} />
      </div>
    </div>
  );
}

function ButtonHelpBar(props) {
  const activate = props.activate;
  const trickKey = props.trickKey
  return (
    <div
      className="bgc-dark3 text-accent h-5 d-flex align-items-center abs w-100 px-2"
      style={{ top: "100%" }}
    >
      <div className="rounded bgc-dark p-1 border-accent w-2 text-center">
        {activate.toUpperCase()}
      </div>
      <span className="ms-1">equip/unequip</span>
      <div className="rounded bgc-dark p-1 border-accent w-2 text-center ms-1">
        {trickKey.toUpperCase()}
      </div>
      <span className="ms-1">trick toggle</span>
    </div>
  );
}

function ItemEntry(props) {
  const item = props.item;
  const player = props.player;
  const vw = item.value / item.weight;
  // const [hover, setHover] = useState(false);

  const activate = props.activate;
  const trickKey = props.trickKey

  //const itemIndex = props.itemIndex;

  const star = (
    <svg className="ms-1" height="20" width="20">
      <polygon
        points="10,1 3,20 20,8 0,8 17,20"
        style={{
          fill: "gold",
          stroke: "5",
          strokeWidth: "1",
          fillRule: "nonzero",
        }}
      />
    </svg>
  );

  const dot = (
    <svg height="20" width="20">
      <circle cx="10" cy="10" r="5" fill="white" />
    </svg>
  );

  // const handleEnter = () => {
  //   setHover(true);
  // };
  // const handleLeave = () => {
  //   setHover(false);
  // };

  function findEquippedSlot(slotItem, target) {
    const equippedSlot = slotItem.slots.find((slot) => {
      return slot.equipped === target;
    });

    return equippedSlot;
  }

  const belt = props.items.find((item) => {
    return item.equipped === "belt";
  });

  const back = props.items.find((item) => {
    return item.equipped === "back";
  });

  const handleKeyDown = (e) => {
    // if (hover) {
    if (e.key === activate) {
      const target = item;
      //un-equipping logic
      if (target.equipped) {
        if (target.slotType) {
          const equippedSlot = findEquippedSlot(target.equipped, target);
          delete equippedSlot.equipped;
        }
        if (target.type[0] === "talisman") {
          const talisman = player.talismanSlots.find((slot) => {
            return slot.equipped.id === target.id;
          });
          if (talisman) delete talisman.equipped;
        }
        delete target.equipped;

        //equipping logic
      } else {
        if (!target.slotType) {

          if (!(target.type[0] === "talisman")) {
            const equippedItem = props.items.find((item) => {
              return item.equipped === target.type[1];
            });
            if (equippedItem) {
              delete equippedItem.equipped;
            }
            target.equipped = target.type[1];
            //if it is a talisman
          } else {
            const emptySlotCheck = player.talismanSlots.find((slot) => {
              return slot.equipped === undefined;
            });
            if (emptySlotCheck) {
              const ref = emptySlotCheck;
              ref.equipped = target;
              target.equipped = "talisman";
            }
          }
          props.setUpdate(!props.update);
        } else {
          //if it is equipped on belt or back
          if (belt) {
            // console.log(belt.slots);
            const typeCheck = belt.slots.filter((slot) => {
              return slot.type === target.slotType;
            });
            // console.log(typeCheck, ":", target.slotType);
            if (typeCheck.length >= 0) {
              const emptySlotCheck = typeCheck.find((slot) => {
                return slot.equipped === undefined;
              });
              // console.log(emptySlotCheck);
              if (emptySlotCheck) {
                const ref = emptySlotCheck;
                ref.equipped = target;
                target.equipped = belt;

                props.setUpdate(!props.update);
                return;
              } else {
                props.setUpdate(!props.update);
                return;
              }
            }
          }
          if (back) {
            // console.log(belt.slots);
            const typeCheck = back.slots.filter((slot) => {
              return slot.type === target.slotType;
            });
            // console.log(typeCheck, ":", target.slotType);
            if (typeCheck.length >= 0) {
              const emptySlotCheck = typeCheck.find((slot) => {
                return slot.equipped === undefined;
              });
              // console.log(emptySlotCheck);
              if (emptySlotCheck) {
                const ref = emptySlotCheck;
                ref.equipped = target;
                target.equipped = back;
              } else {
              }
            }
          }
        }
      }
      props.setUpdate(!props.update);
    } else if (e.key === trickKey) {
      const target = item
      if(target.state){
        let stateIndex = target.states.indexOf(target.state)
        stateIndex = cycleArr(target.states, stateIndex)
        target.state = target.states[stateIndex]

        if (target.slotType && target.equipped) {
          const equippedSlot = findEquippedSlot(target.equipped, target);
          delete equippedSlot.equipped;
          delete target.equipped;
        }
        
        
      }
      props.setUpdate(!props.update);
    }

  };

  // console.log(item.name,hover)

  let equippedVar = item?.equipped;
  if (equippedVar === "belt") equippedVar = undefined;
  if (equippedVar?.name) {
    if (equippedVar?.id === belt?.id || equippedVar?.id === back?.id) {
      equippedVar = undefined;
    }
  }

  return (
    <tr
      // onMouseEnter={handleEnter}
      // onMouseLeave={handleLeave}
      onKeyDown={handleKeyDown}
      tabIndex="0"
    >
      <td className="d-flex align-items-center">
        {item.name}
        {item.amount && `s (${item.amount})`}
        {item.fav && star}
        {item.equipped && <span className="ms-1">{dot}</span>}
        {item.state && `(${item[item.state].name})`}
        {equippedVar?.name && `(${equippedVar.name})`}
      </td>
      <td>
        {item.damage}
        {!item.damage && "-"}
      </td>
      <td>{item.weight}</td>
      <td>{item.value}</td>
      <td>{vw.toFixed(2)}</td>
    </tr>
  );
}
