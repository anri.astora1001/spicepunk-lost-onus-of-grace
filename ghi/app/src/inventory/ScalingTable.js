export default function ScalingTable(props) {
    const item = props.item;
    if (!item.scaling) return;
  
    const scaling = item.scaling;
  
    return (
      <table className="table text-accent p-0">
        <thead>
          <tr>
            <th className="text-center">Prow</th>
            <th className="text-center">Cant</th>
            <th className="text-center">Und</th>
            <th className="text-center">Lcnt</th>
            <th className="text-center">Ins</th>
            <th className="text-center">Agl</th>
          </tr>
        </thead>
        <tbody>
          <tr className="">
            <td className="text-center">
              {scaling.prowess}
              {!scaling.prowess && "-"}
            </td>
            <td className="text-center">
              {scaling.cantillation}
              {!scaling.cantillation && "-"}
            </td>
            <td className="text-center">
              {scaling.understanding}
              {!scaling.understanding && "-"}
            </td>
            <td className="text-center">
              {scaling.licentiousness}
              {!scaling.licentiousness && "-"}
            </td>
            <td className="text-center">
              {scaling.insight}
              {!scaling.insight && "-"}
            </td>
            <td className="text-center">
              {scaling.agility}
              {!scaling.agility && "-"}
            </td>
          </tr>
        </tbody>
      </table>
    );
  }
  