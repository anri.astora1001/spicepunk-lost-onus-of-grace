import MainButton from "../common/MainButton";
import { useState, useEffect } from "react";
import { jumpCheck, climbCheck } from "../common/skill-checks-util";
import {
  DialogueBox,
  DialogueEntry,
  QuestEntry,
} from "../dialogue/DialogeComps";
import interact from "../common/World/interact";
import updateQuests from "../common/Quests/updateQuests";
import GameMap from "./Map/GameMap";
import CastSpellMenu from "./Spells/CastSpellMenu";

export default function MainScreen() {
  const [update, setUpdate] = useState(true);
  const [dimensions, setDimensions] = useState({});
  const [player, setPlayer] = useState({
    name: "player",
    x: 0,
    y: 0,

    state: "",
    height: 2,
    status: {
      maxHealth: 100,
      health: 25,

      maxMana: 100,
      mana: 100,

      thirst: 1,
    },

    position: { x: 0, y: 0, elevation: 2 },

    items: [],

    powerWords: {
      nouns: [
        {
          name: "pyra",
          damageType: "fire",
          baseDamage: "1d10",
          school: "arcane",
        },
        {
          name: "atar",
          damageType: "holy-fire",
          baseDamage: "1d10",
          school: "cantillation",
        },
        {
          name: "kryha",
          damageType: "ice",
          baseDamage: "1d10",
          school: "arcane",
        },
      ],
      verbs: [
        { name: "velos", target: "single", range: "9", school: "arcane" },
        { name: "tir", target: "single", range: "9", school: "cantillation" },
      ],
      adverbs: [
        { name: "sigi", tags: "silence", description: "makes spell silent" },
      ],
    },
  });

  const [quests, setQuests] = useState([
    {
      QuestId: "test0",
      name: "Collect Flowers Test",
      progress: "0/5",
      description: "collect 5 flowers",
      type: "item",
    },
  ]);

  const [leftFocus, setLeftFocus] = useState("gameText");

  const [mapData, setMapData] = useState({
    NPCs: [
      {
        name: "Test Dummy",
        tags: "training-dummy",
        height: 2,
        position: { x: 1, y: 0, elevation: 2 },
        icon: {
          color: "red",
        },
        status: {
          maxHealth: 100,
          health: 100,
        },
      },
    ],
    tileSize: 32,
    tiles: [
      [
        { elevation: 2, terrainTypes: "" },
        { elevation: 2, terrainTypes: "" },
        { elevation: 2, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
      ],
      [
        { elevation: 4, terrainTypes: "" },
        { elevation: 2, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
      ],
      [
        { elevation: 2, terrainTypes: "" },
        { elevation: 2, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
      ],
      [
        { elevation: 2, terrainTypes: "" },
        {
          elevation: 2,
          terrainTypes: "",
          interact: "pick flower",
          action: "pick flower",
          amount: 5,
        },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
      ],
      [
        { elevation: 1, terrainTypes: "" },
        { elevation: 2, terrainTypes: "" },
        {
          elevation: 0,
          terrainTypes: "",
          interact: "pick flower",
          action: "pick flower",
          amount: 2,
        },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
      ],
      [
        { elevation: 0, terrainTypes: "" },
        { elevation: 2, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, blocker: true, terrainTypes: "blocker water" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
      ],
      [
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
      ],
      [
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
        { elevation: 0, terrainTypes: "" },
      ],
    ],
  });
  const tileSize = mapData.tileSize;
  const [moveAction, setMoveAction] = useState(["", ""]);
  const [interactAction, setInteractAction] = useState(["", ""]);
  const [gameText, setGameText] = useState([
    <DialogueEntry speaker="Narrator">
      You have entered the testing grounds
    </DialogueEntry>,
  ]);

  function moveRight() {
    const target = player;
    const pos = player.position;

    target.x += tileSize;
    target.position.x += 1;
    target.position.elevation = mapData.tiles[pos.y][pos.x].elevation;
    setMoveAction(["", ""]);
  }

  function moveLeft() {
    const target = player;
    const pos = player.position;

    target.x -= tileSize;
    target.position.x -= 1;
    target.position.elevation = mapData.tiles[pos.y][pos.x].elevation;
    setMoveAction(["", ""]);
  }

  function moveUp() {
    const target = player;
    const pos = player.position;

    target.y -= tileSize;
    target.position.y -= 1;
    target.position.elevation = mapData.tiles[pos.y][pos.x].elevation;
    setMoveAction(["", ""]);
  }

  function moveDown() {
    const target = player;
    const pos = player.position;

    target.y += tileSize;
    target.position.y += 1;
    target.position.elevation = mapData.tiles[pos.y][pos.x].elevation;
    setMoveAction(["", ""]);
  }

  function moveDirection(direction) {
    switch (direction) {
      default:
      case "right":
        moveRight();
        break;
      case "left":
        moveLeft();
        break;
      case "up":
        moveUp();
        break;
      case "down":
        moveDown();
    }
    const tile = getTile();
    if (tile.interact) {
      setInteractAction([tile.interact, tile.action]);
    } else {
      setInteractAction(["", ""]);
    }
  }

  function getTile(direction) {
    const pos = player.position;
    switch (direction) {
      default:
        return mapData.tiles[pos.y][pos.x];
      case "right":
        return mapData.tiles[pos.y][pos.x + 1];
      case "left":
        return mapData.tiles[pos.y][pos.x - 1];
      case "up":
        return mapData.tiles[pos.y - 1][pos.x];
      case "down":
        return mapData.tiles[pos.y + 1][pos.x];
    }
  }

  const handleLeftFocus = (e) => {
    if (leftFocus === "gameText") {
      const focus = e.target.value;
      setLeftFocus(focus);
    } else {
      setLeftFocus("gameText");
    }
  };

  const handleClimb = () => {
    const target = player;
    const pos = player.position;
    const text = gameText;

    const height = getTile(moveAction[1]).elevation;
    console.log(height);
    const check = climbCheck(height);
    target.state = "";
    if (check === "success") {
      text.push(
        <DialogueEntry speaker="Narrator">
          You successfully climbed up the ledge.
        </DialogueEntry>
      );
      moveDirection(moveAction[1]);
    } else {
      const damage = check * 3;
      target.status.health -= damage;
      text.push(
        <DialogueEntry speaker="Narrator">
          You failed to climb up, falling back down in the process. You took{" "}
          {damage} fall damage
        </DialogueEntry>
      );
    }

    setUpdate(!update);
  };

  const handleJump = () => {
    const target = player;
    const pos = player.position;
    const text = gameText;

    const diff = pos.elevation - getTile(moveAction[1]).elevation - 2;
    const check = jumpCheck(diff);
    if (check === "fail") {
      const damage = diff * 5 + 3;
      target.status.health -= damage;
      text.push(
        <DialogueEntry speaker="Narrator">
          {" "}
          You jumped but injured yourself in the process. You took {damage} fall
          damage.
        </DialogueEntry>
      );
    }
    text.push(
      <DialogueEntry speaker="Narrator">
        You jumped safely off the ledge
      </DialogueEntry>
    );
    moveDirection(moveAction[1]);
    setUpdate(!update);
  };

  const handleSwim = () => {
    const target = player;
    //const pos = player.position;
    const text = gameText;

    text.push(
      <DialogueEntry speaker="Narrator">
        you went for a swim. The water was warm and clear.
      </DialogueEntry>
    );

    target.state = "swimming";
    moveDirection(moveAction[1]);
    setUpdate(!update);
  };

  const handleMovement = (e) => {
    const direction = e.target.value;
    const target = player;
    const pos = player.position;
    let tile = getTile(direction);
    if (tile === undefined) {
      return;
    }

    let terrainTypes = tile.terrainTypes.split(" ");

    if (player.state === "") {
      if (!terrainTypes.includes("blocker")) {
        const tileElevation = tile.elevation;
        const diff = Math.abs(pos.elevation - tileElevation);
        if (diff > 1) {
          console.log("entered elevation difference");
          pos.elevation > tileElevation
            ? setMoveAction(["jump", direction])
            : setMoveAction(["climb", direction]);
          return;
        }
        moveDirection(direction);
      } else {
        if (terrainTypes.includes("water")) {
          setMoveAction(["swim", direction]);
        }
      }
    } else if (player.state === "swimming") {
      if (terrainTypes.includes("water")) {
        moveDirection(direction);
      } else if (!terrainTypes.includes("blocker")) {
        setMoveAction(["climb", direction]);
      }
    }
    setUpdate(!update);
  };

  const handleInteract = () => {
    const tile = getTile();
    const text = gameText;
    const interactText = interact(interactAction[1], player, tile);
    updateQuests(quests, player, "items");
    // console.log(interactText)
    text.push(<DialogueEntry speaker="Narrator">{interactText}</DialogueEntry>);
    setUpdate(!update);
  };

  // console.log(player.items)
  //console.log(leftFocus);

  return (
    <div className="container-fluid bgc-dark vh-100 ">
      {update && <div></div>}
      <div className="row  h-70 bgc-dark2 ">
        <div className="col bgc-dark h-100">
          {leftFocus === "gameText" && <DialogueBox>{gameText}</DialogueBox>}
          {leftFocus === "quests" && (
            <div className="overflow-auto">
              <h1 className="text-accent">Quests</h1>
              {quests.map((quest, i) => {
                return (
                  <DialogueBox>
                    <QuestEntry key={`quest:${i}`} quest={quest} />
                  </DialogueBox>
                );
              })}
            </div>
          )}
          {leftFocus === "spells" && (
            <CastSpellMenu
              powerWords={player.powerWords}
              mapData={mapData}
              player={player}
            />
          )}
        </div>
        <div className="col bgc-dark3  p-0">
          <GameMap
            player={player}
            sendDimensions={setDimensions}
            mapData={mapData}
          />
        </div>
      </div>
      <div className="row h-10 bgc-dark3">
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton>
          <button
            className="bgc-accent btn-c"
            onClick={handleLeftFocus}
            value="spells"
          >
            <span className="mx-auto text-black">Cast Spell</span>
          </button>
        </MainButton>
      </div>
      <div className="row h-10 bgc-dark3">
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton>
          {interactAction !== ["", ""] && (
            <button
              className="bgc-accent btn-c"
              value={interactAction[1]}
              onClick={handleInteract}
            >
              <span className="mx-auto text-black">{interactAction[0]}</span>
            </button>
          )}
        </MainButton>
        <MainButton>
          <button
            className="bgc-accent btn-c"
            onClick={handleMovement}
            value="up"
          >
            <span className="mx-auto text-black"> Up</span>
          </button>
        </MainButton>
        <MainButton>
          {moveAction[0] === "climb" && (
            <div className="bgc-accent btn-c" onClick={handleClimb}>
              <span className="mx-auto text-black">Climb</span>
            </div>
          )}
          {moveAction[0] === "jump" && (
            <div className="bgc-accent btn-c" onClick={handleJump}>
              <span className="mx-auto text-black">Jump</span>
            </div>
          )}
          {moveAction[0] === "swim" && (
            <div className="bgc-accent btn-c" onClick={handleSwim}>
              <span className="mx-auto text-black">Swim</span>
            </div>
          )}
          {moveAction[0] === "" && <div className="bgc-accent btn-c"></div>}
        </MainButton>
      </div>
      <div className="row h-10 bgc-dark3">
        <MainButton>
          <button
            className="bgc-accent btn-c"
            onClick={handleLeftFocus}
            value="quests"
          >
            <span className="mx-auto text-black">Quests</span>
          </button>
        </MainButton>
        <MainButton />
        <MainButton />
        <MainButton>
          <button
            className="bgc-accent btn-c"
            onClick={handleMovement}
            value="left"
          >
            <span className="mx-auto text-black"> Left</span>
          </button>
        </MainButton>
        <MainButton>
          <button
            className="bgc-accent btn-c"
            onClick={handleMovement}
            value="down"
          >
            <span className="mx-auto text-black"> Down</span>
          </button>
        </MainButton>
        <MainButton>
          <button
            className="bgc-accent btn-c"
            onClick={handleMovement}
            value="right"
          >
            <span className="mx-auto text-black"> Right</span>
          </button>
        </MainButton>
      </div>
    </div>
  );
}
