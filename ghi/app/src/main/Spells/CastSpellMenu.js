import { DialogueBox } from "../../dialogue/DialogeComps";
import { useState } from "react";
import detects from "../Map/detection";

export default function CastSpellMenu(props) {
  const powerWords = props.powerWords;

  const [update, setUpdate] = useState(true);
  const [focus, setFocus] = useState("Nouns");

  const [spell, setSpell] = useState({
    noun: undefined,
    verb: undefined,
    adverb: undefined,
  });

  const handleCast = () => {
    setFocus("Targets");
  };

  const reviewToggle = () => {
    setFocus("Review");
  };

  return (
    <DialogueBox className={"h-95"}>
      {update && <div></div>}
      <div className="d-flex flex-row">
        <span className="text-accent">Spell: </span>
        {spell.noun && (
          <span className="text-accent ms-1">{spell.noun.name}</span>
        )}
        {spell.verb && <span className="text-accent">{spell.verb.name}</span>}
        {spell.adverb && (
          <span className="text-accent ms-1">{spell.adverb.name}</span>
        )}
        {spell.noun && spell.verb && (
          <button className="btn-c-small ms-1" onClick={handleCast}>
            Cast
          </button>
        )}
        {spell.noun && spell.verb && (
          <button className="btn-c-small ms-1" onClick={reviewToggle}>
            Review
          </button>
        )}
      </div>
      <h3 className="text-accent">{focus}:</h3>
      {focus === "Nouns" &&
        powerWords.nouns.map((noun, i) => {
          return (
            <DialogueBox key={`spell-noun:${i}`} className={""}>
              <NounEntry noun={noun} spell={spell} setFocus={setFocus} />
            </DialogueBox>
          );
        })}
      {focus === "Verbs" &&
        powerWords.verbs.map((verb, i) => {
          return (
            <DialogueBox key={`spell-verbs:${i}`} className={""}>
              <VerbEntry verb={verb} spell={spell} setFocus={setFocus} />
            </DialogueBox>
          );
        })}
      {focus === "Adverbs" &&
        powerWords.adverbs.map((adverb, i) => {
          return (
            <DialogueBox key={`spell-adverbs:${i}`} className={""}>
              <AdverbEntry adverb={adverb} spell={spell} setFocus={setFocus} />
            </DialogueBox>
          );
        })}
      {focus === "Review" && <SpellReview spell={spell} />}
      {focus === "Targets" && (
        <TargetsMenu
          spell={spell}
          mapData={props.mapData}
          player={props.player}
        />
      )}
    </DialogueBox>
  );
}

function TargetsMenu(props) {
  const spell = props.spell;
  const mapData = props.mapData;
  const player = props.player;
  const [spellTarget, setTarget] = useState(undefined);

  const targets = mapData.NPCs.filter((npc) => {
    return detects(player, npc, mapData.tiles);
  });

  if (targets.length === 0 && !spellTarget) {
    return (
      <div className="text-accent">
        <span>No Valid Quick Targets</span>
      </div>
    );
  }

  const changeTarget = (target) => {
    setTarget(target);
  };

  return (
    <div>
      {!spellTarget &&
        targets.map((target, i) => {
          return (
            <DialogueBox
              key={`spell-target:${i}`}
              onClick={() => changeTarget(target)}
            >
              <div className="text-accent d-flex flex-row">
                <span>
                  {target.name} Health:{target.status.health}/
                  {target.status.maxHealth}
                </span>
              </div>
            </DialogueBox>
          );
        })}
      {spellTarget && <div></div>}
    </div>
  );
}

function SpellReview(props) {
  const spell = props.spell;

  return (
    <div>
      <div className="text-accent d-flex flex-row">
        <span>Damage:</span>
        <span className="ms-1">
          {spell.noun.damageType} {spell.noun.baseDamage}
        </span>
      </div>
      <div className="text-accent d-flex flex-row">
        <span>Range:</span>
        <span className="ms-1">{spell.verb.range}</span>
      </div>
      <div className="text-accent d-flex flex-row">
        <span>Target:</span>
        <span className="ms-1">{spell.verb.target}</span>
      </div>
      {spell.adverb && (
        <div className="text-accent d-flex flex-row">
          <span>Adverb:</span>
          <span className="ms-1">{spell.verb.description}</span>
        </div>
      )}
    </div>
  );
}

function AdverbEntry(props) {
  const adverb = props.adverb;
  const spell = props.spell;

  const pickVerb = () => {
    spell.adverb = adverb;
    props.setFocus("Review");
  };

  return (
    <div className="text-accent d-flex flex-row">
      <button className=" btn-c-small" onClick={pickVerb}>
        +
      </button>
      <span className="ps-2">{adverb.name}</span>
    </div>
  );
}

function VerbEntry(props) {
  const verb = props.verb;
  const spell = props.spell;

  const pickVerb = () => {
    spell.verb = verb;
    props.setFocus("Adverbs");
  };

  return (
    <div className="text-accent d-flex flex-row">
      <button className=" btn-c-small" onClick={pickVerb}>
        +
      </button>
      <span className="ps-2">{verb.name}</span>
    </div>
  );
}

function NounEntry(props) {
  const noun = props.noun;
  const spell = props.spell;

  const pickNoun = () => {
    spell.noun = noun;
    props.setFocus("Verbs");
    // props.setUpdate(!props.update);
  };

  return (
    <div className="text-accent d-flex flex-row">
      <button className=" btn-c-small" onClick={pickNoun}>
        +
      </button>
      <span className="ps-2">{noun.name}</span>
      <span className="ps-2">
        Damage: {noun.damageType} {noun.baseDamage}
      </span>
      <span className="ps-2">School: {noun.school}</span>
    </div>
  );
}
