import { useState, useEffect } from "react";
import MainButton from "../../common/MainButton";
import detects from "./detection";

export default function GameMap(props) {
  const player = props.player;
  const [dimensions, setDimensions] = useState({});
  const mapData = props.mapData;
  const [elevationToggle, setElevationToggle] = useState(false);

  const [focusedTargets, setFocusedTargets] = useState([]);

  const [update, setUpdate] = useState(true);

  const findDimensions = () => {
    const map = document.getElementById("map");
    const height = map.clientHeight;
    const width = map.clientWidth;

    setDimensions({ height: height, width: width });
    props.sendDimensions({ height: height, width: width });
  };

  useEffect(() => {
    findDimensions();
  }, [document.getElementById("map")]);

  const handleElevationToggle = () => {
    setElevationToggle(!elevationToggle);
  };

  // console.log(dimensions);

  return (
    <div className="h-100 ">
      <div className="row h-5 w-100 g-0">
        {update && <div></div>}
        <MainButton>
          <div className="bgc-accent btn-c" onClick={handleElevationToggle}>
            <span className="mx-auto text-black">Elevation</span>
          </div>
        </MainButton>
      </div>
      <div className="row h-5 w-100 g-0">
        <div className="col">
          <div
            className="bgc-health h-100 mx-auto d-flex align-items-center justify-content-center rounded"
            style={{
              width: `${
                (player.status.health / player.status.maxHealth) * 100
              }%`,
            }}
          >
            {player.status.health}/{player.status.maxHealth}
          </div>
        </div>
        <div className="col">
          <div
            className="bgc-mana h-100 mx-auto d-flex align-items-center justify-content-center rounded text-white"
            style={{
              width: `${(player.status.mana / player.status.maxMana) * 100}%`,
            }}
          >
            {player.status.mana}/{player.status.maxMana}
          </div>
        </div>
      </div>
      <div className="row g-0 border d-flex align-items-center h-90 rel">
        <div className="abs ps-2" style={{ top: "0px", left: "0px" }}>
          {focusedTargets.map((target, i) => {
            return <FocusedTarget key={`ft:${i}`} target={target} />;
          })}
        </div>
        <div
          className="bgc-grass rel mx-auto"
          style={{
            // height: `${dimensions.height}px`,
            // width: `${dimensions.width}px`,
            height: "256px",
            width: "256px",
          }}
        >
          <img
            id="map"
            className="rel"
            src={`/images/testingMap.png`}
            alt="the map"
            style={{}}
          ></img>

          <ElevationFog
            mapData={mapData}
            player={player}
            toggle={elevationToggle}
          />
          {mapData.NPCs.map((npc, i) => {
            return (
              <NPC_ICON
                key={`npc:${i}`}
                npc={npc}
                tileSize={mapData.tileSize}
                ft={focusedTargets}
                setFT={setFocusedTargets}
                update={update}
                setUpdate={setUpdate}
                player={player}
                tiles={mapData.tiles}
              />
            );
          })}
          <PlayerIcon player={player} />
        </div>
      </div>
    </div>
  );
}

function FocusedTarget(props) {
  const target = props.target;

  return (
    <div className="text-accent">
      <div>Name: {target.name}</div>
      <div>
        Health: {target.status.health} / {target.status.maxHealth}
      </div>
    </div>
  );
}

function PlayerIcon(props) {
  const playerIcon = (
    <svg height="32" width="32">
      <circle cx="16" cy="16" r="14" fill="blue" />
    </svg>
  );
  const player = props.player;
  const pos = player.position;

  let x = 0;
  let y = 0;

  if (pos.x) x = player.x;
  if (pos.y) y = player.y;

  return (
    <div className="abs" style={{ top: `${y}px`, left: `${x}px` }}>
      {playerIcon}
    </div>
  );
}

function NPC_ICON(props) {
  const npc = props.npc;
  const pos = npc.position;
  const tileSize = props.tileSize;
  const ft = props.ft;

  const tiles = props.tiles;

  const x = pos.x * tileSize;
  const y = pos.y * tileSize;

  const focused = ft.includes(npc);
  const player = props.player;

  //trig time
  
  const render = detects(player, npc, tiles)

  

  const Icon = (
    <svg height="32" width="32">
      <circle cx="16" cy="16" r="14" fill={`${npc.icon.color}`} />
    </svg>
  );

  const handleFocus = () => {
    if (!focused) {
      ft.push(npc);
    } else {
      const temp = ft.filter((target) => {
        console.log(target === npc);
        return target !== npc;
      });
      props.setFT(temp);
    }
    props.setUpdate(!props.update);
  };

  useEffect(() => {
    if (render) {
      const iconDiv = document.getElementById("icon");
      if (focused) {
        iconDiv.classList.add("border");
      } else {
        iconDiv.classList.remove("border");
      }
    }
  });

  if (!render) return;

  return [
    <div
      id="icon"
      className="abs cursor-pointer"
      style={{ top: `${y}px`, left: `${x}px` }}
      onClick={handleFocus}
    >
      {Icon}
    </div>,
    <HoverBar height={20} position={{ x: x, y: y }}>
      {npc.name}
    </HoverBar>,
  ];
}

function HoverBar(props) {
  const pos = props.position;
  const height = props.height;
  const [width, setWidth] = useState(0);

  const findWidth = () => {
    const hoverBar = document.getElementById("hover-bar");
    setWidth(hoverBar.clientWidth / 3);
  };

  useEffect(() => {
    findWidth();
  }, [document.getElementById("hover-bar")]);

  return (
    <div
      id="hover-bar"
      className="abs bgc-dark-sheer text-accent d-flex align-items-center"
      style={{
        height: `${height}px`,
        top: `${pos.y - height}px`,
        left: `${pos.x - width}px`,
      }}
    >
      <div className="container ">{props.children}</div>
    </div>
  );
}

function ElevationFog(props) {
  const mapData = props.mapData;
  const player = props.player;

  return mapData.tiles.map((row, y) => {
    return row.map((col, x) => {
      if (player.position.elevation === col.elevation)
        return (
          <ElevationFogTile
            key={`fog:${y}:${x}`}
            pos={{ y: y * mapData.tileSize, x: x * mapData.tileSize }}
            tileSize={mapData.tileSize}
            elevation={col.elevation}
            off={true}
            toggle={props.toggle}
          />
        );

      const difference =
        Math.abs(player.position.elevation - col.elevation) - 1;
      return (
        <ElevationFogTile
          key={`fog:${y}:${x}`}
          pos={{ y: y * mapData.tileSize, x: x * mapData.tileSize }}
          tileSize={mapData.tileSize}
          elevation={col.elevation}
          difference={difference}
          toggle={props.toggle}
        />
      );
    });
  });
}

function ElevationFogTile(props) {
  const pos = props.pos;
  const tileSize = props.tileSize;

  let diff = 0;
  if (props.difference) diff = props.difference;

  let shadow = `inset 0 0 0 9999px rgba(80,80, 80 ,${0.4 + diff * 0.1})`;
  if (props.off) shadow = "";

  return (
    <div
      className="abs d-flex align-items-center justify-content-center"
      style={{
        top: `${pos.y}px`,
        left: `${pos.x}px`,
        height: `${tileSize}px`,
        width: `${tileSize}px`,
        boxShadow: shadow,
      }}
    >
      {props.toggle && props.elevation}
    </div>
  );
}
