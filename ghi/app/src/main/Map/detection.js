function findDirection(sPos, tPos) {
  //general down
  if (tPos.y > sPos.y) {
    if (tPos.x === sPos.x) return "down";
    if (tPos.x > sPos.x) return "down-right";
    return "down-left";
  }
  //general up
  else if (tPos.y < sPos.y) {
    if (tPos.x === sPos.x) return "up";
    if (tPos.x > sPos.x) return "up-right";
    return "up-left";
  }
  //same Y
  else {
    if (tPos.x > sPos.x) return "right";
    return "left";
  }
}

function getTile(sPos, direction, tiles, b, a) {
  const y = Math.round(b);
  const x = Math.round(a);
//   console.log(
//     "direction: ",
//     direction,
//     " x: ",
//     x,
//     " y: ",
//     y,
//     " pX: ",
//     sPos.x,
//     " pY: ",
//     sPos.y
//   );
  switch (direction) {
    default:
      return tiles[sPos.y][sPos.x];
    case "up-right":
      return tiles[sPos.y - y][sPos.x + x];
    case "down-right":
      return tiles[sPos.y + y][sPos.x + x];
    case "up-left":
      return tiles[sPos.y - y][sPos.x - x];
    case "down-left":
      return tiles[sPos.y + y][sPos.x - x];
    case "up":
      return tiles[sPos.y - y][sPos.x];
    case "down":
      return tiles[sPos.y + y][sPos.x];
    case "right":
      return tiles[sPos.y][sPos.x + x];
    case "left":
      return tiles[sPos.y][sPos.x - x];
  }
}

function findDistance(direction, xDistance, yDistance) {
  let distance = 0;
  if (direction === "down" || direction === "up") {
    distance = yDistance;
  } else if (direction === "right" || direction === "left") {
    distance = xDistance;
  } else {
    distance = Math.sqrt(xDistance * xDistance + yDistance * yDistance);
  }
  return distance;
}

export default function detects(seer, target, tiles) {
  let detect = true;

  const sPos = seer.position;
  const tPos = target.position;

  const direction = findDirection(sPos, tPos);

  const yDistance = Math.abs(sPos.y - tPos.y);
  const xDistance = Math.abs(sPos.x - tPos.x);

  const distance = findDistance(direction, xDistance, yDistance);

  //   console.log("distance: ", distance);
  //   console.log("direction: ", direction);

  const sElevation = sPos.elevation;
  const eyeHeight = sElevation + seer.height * 0.95;

  let a = 0;

  let vDirection = "";
  if (sPos.elevation >= tPos.elevation) {
    a = eyeHeight;
    vDirection = "below";
  } else {
    a = tPos.elevation + target.height - eyeHeight;
    vDirection = "above";
  }

  const A = Math.atan(a / distance);

  const ht_a = xDistance;
  const ht_b = yDistance;
  const ht_A = Math.atan(ht_a / ht_b);

  const segment = distance / Math.floor(distance);
  for (let i = 1; i <= distance; i++) {
    const  b = i * segment;
    const sightLineHeight = b * Math.tan(A);
    const h_c = i * segment;
    const h_B = Math.PI / 2 - ht_A;

    const h_b = h_c * Math.sin(h_B);
    const h_a = h_c * Math.sin(ht_A);

    const tile = getTile(sPos, direction, tiles, h_b, h_a);

    if (sElevation === tPos.elevation) {
    // console.log("sLH: ", sightLineHeight, "tileE", tile.elevation);
      if (tile.elevation > eyeHeight) {
        detect = false;
        break;
      }
    } else {
      if (vDirection === "below") {
        // console.log("sLH: ", sightLineHeight, "tileE", tile.elevation);
        if (tile.elevation + 0.11 > sightLineHeight) {
          detect = false;
          break;
        }
      } else {
        // console.log("sLH: ", sightLineHeight + eyeHeight, "tileE", tile.elevation);
        if (tile.elevation + 0.3 > sightLineHeight + eyeHeight) {
          detect = false;
          break;
        }
      }
    }
  }

  return detect;
}
