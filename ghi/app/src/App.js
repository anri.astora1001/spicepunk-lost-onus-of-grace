import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import StartMenu from './start/StartMenu';
import CharCreatePage from './start/Character-Creation/CharCreatePage';
import DialoguePage from './dialogue/DialoguePage';
import InventoryPage from './inventory/InventoryPage';
import MainScreen from './main/MainScreen';

function App() {
    return (
      <BrowserRouter>
          <Routes>
            <Route path="/" element={<StartMenu />} />
            <Route path="/newgame/character-creation" element={<CharCreatePage/>} />
            <Route path="test/dialogue" element={<DialoguePage />} />
            <Route path="test/inventory" element={<InventoryPage />} />
            <Route path="test/main" element={<MainScreen />} />
          </Routes>
      </BrowserRouter>
    );
}

export default App;
