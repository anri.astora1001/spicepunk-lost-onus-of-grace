import {
  VarText,
  GenderNoun,
  AgeAdj,
  HeightAdj,
} from "../common/character-text-util";
import RaceText from "../common/RaceText";

export default function GameText(props) {
  const text = props.children;

  const textArray = text.split("@");

  return (
    <span>
      {textArray.map((text) => {
        if (text[0] === "^") {
          const res = text.replace("^", "");
          const code = res.split(":");
          const id = code[0];
          const data = code[1];

          if (id === "VT" || id === "VarText") {
            return <VarText var={data} />;
          } else if (id === "RT" || id === "RaceText") {
            return <RaceText race={data} />;
          } else if (id === "GN" || id === "GenderNoun") {
            return <GenderNoun pronoun={data} />;
          } else if (id === "AA" || id === "AgeAdj") {
            return <AgeAdj age={data} />;
          } else if (id === "HA" || id === "HeightAdj") {
            return <HeightAdj height={data} />;
          }

          return (
            <span>
              {data}: {id}
            </span>
          );
        }
        return <span>{text}</span>;
      })}
    </span>
  );
}
