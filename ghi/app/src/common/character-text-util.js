import { adjAge, adjHeight, genderNoun } from "./adjectives-util";

function VarText(props) {
  if (!props.var) return;
  return <span>{props.var}</span>;
}

function GenderNoun(props){
    if(!props.pronoun) return
    return <span>{genderNoun(props.pronoun)}</span>
}

function AgeAdj(props) {
  if (!props.age) return;
  return <span>{adjAge(props.age)}</span>;
}

function HeightAdj(props){
    if(!props.height) return
    return <span>{adjHeight(props.height)}</span>
}

export { VarText, GenderNoun, AgeAdj, HeightAdj };
