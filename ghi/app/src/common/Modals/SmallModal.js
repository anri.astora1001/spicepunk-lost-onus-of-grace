

export default function SmallModal(props) {
    const text = props?.text
    
    if(text === undefined || !props.display) return
    return (
        <div className="relative-modal px-1 ">
            {text}
        </div>
    )
}