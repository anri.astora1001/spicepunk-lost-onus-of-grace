import StatModalBody from "./ModalBodies/StatModalBody";
import CombatModalBody from "./ModalBodies/CombatModalBody";
import RaceModalBody from "./ModalBodies/RaceModalBody";
import NPCInspectModalBody from "./ModalBodies/NPCInspectModalBody";
import { useState, useEffect } from "react";

export default function LargeModal(props) {
  const [type, setType] = useState(props.type);
  const [body, setBody] = useState(<div>Loading...</div>);
  const [title, setTitle] = useState(props.type[1]);
  const [backArray, setBackArray] = useState([]);
  const [backButton, setBackButton] = useState(<div></div>);

  const getComponents = () => {
    if (type[0] === "stat") {
      setBody(
        <StatModalBody
          changeTitle={setTitle}
          changeBackArray={setBackArray}
          changeType={setType}
          stat={type[1]}
        />
      );
    } else if (type[0] === "combat") {
      setBody(<CombatModalBody />);
    } else if (type[0] === "race") {
      setBody(
        <RaceModalBody
          changeTitle={setTitle}
          changeBackArray={setBackArray}
          changeType={setType}
          race={type[1]}
        />
      );
    } else if( type[0] === "npc-inspect"){
        setBody(
            <NPCInspectModalBody
              changeTitle={setTitle}
              changeBackArray={setBackArray}
              changeType={setType}
              type={type}
              data={props.data}
            />
          );
    }

    if (backArray.length === 0) {
      setBackButton(<div></div>);
    } else {
      setBackButton(
        <BackButton
          backArray={backArray}
          changeTitle={setTitle}
          changeType={setType}
          type={type}
          changeBackArray={setBackArray}
        />
      );
    }
  };

  useEffect(() => {
    getComponents();
  }, [type]);

  const closeButton = () => {
    setTitle(props.type[1]);
    setType(props.type);
    props.displayToggle(false);
  };

  if (!props.display) return;
  return (
    <div className="fixed-modal ">
      <div className="row w-100 text-center mt-2 g-0 border-bottom-c-accent pb-1">
        <div className="col-sm-1 d-flex ps-2 mb-2 ">{backButton}</div>
        <h4 className="col text-accent"> {title} </h4>
        <div
          className="d-flex justify-content-end pe-2 pt-1"
          style={{ width: "50px" }}
        >
          <div className="btn-close  bgc-accent" onClick={closeButton}></div>
        </div>
      </div>
      <div className="row g-0  px-3 pt-2">{body}</div>
    </div>
  );
}

function BackButton(props) {
  const handleClick = () => {
    const temp = props.type;
    props.changeTitle(`${props.backArray[1]}`);
    props.changeType(props.backArray);
    props.changeBackArray(temp);
  };

  return (
    <div
      className="bgc-accent text-black rounded p-1 py-auto"
      onClick={handleClick}
    >
      Back
    </div>
  );
}
