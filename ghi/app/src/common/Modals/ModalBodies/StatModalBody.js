
export default function StatModalBody(props){
    const stat = props?.stat  
    return (
        <div>
            <HeaderText stat={stat} props={props} />
            <div className="row mt-2">
                <SkillText props={props} />
            </div>
        </div>
    )
}

function HeaderText(props){
    const prop = props.props
    const stat = prop.stat

    if(stat === "PECULIAR") return <div>Test</div>
    if(stat === "Prowess") {
        return (
            <div>
                Prowess is one of the 2 martial 
                <PECULIARLink props={prop} /> 
                stats that determines your effectiveness with martial weapons. 
                Prowess also effects how quickly you learn and chances of succeeding Prowess affected skills. 
                All 
                <PECULIARLink props={prop} /> 
                stats have saving throws associated with them which are used to determine 
                the likely-hood of getting out of sticky situations.
                They typically involve you getting stuck and using your strength to potentially get out. 
                For example being grappled by a monstrous plant in their vines.
            </div>
        )
    }
    return "Stat Body Modal"
}

function SkillText(props){
    const prop = props.props
    const stat = prop.stat
    if(stat === "PECULIAR") return
    return (
        <h4>{stat} affected Skills:</h4>
    )

}

function CombatLink(props){
    const prop = props.props
    const handleClick = ()=>{
        prop.changeTitle("Combat")
        prop.changeType(["combat","Combat"])
        prop.changeBackArray(["stat",`${prop.stat}`])
    }

    return (
        <span onClick={handleClick}>Test Combat Link</span>
    )
}

function PECULIARLink(props){
    const prop = props.props
    const handleClick = ()=>{
        prop.changeTitle("PECULIAR")
        prop.changeType(["stat","PECULIAR"])
        prop.changeBackArray(["stat",`${prop.stat}`])
    }

    return(
        <span onClick={handleClick} className="PECULIAR-text" >
            <span> P</span>
            <span>E</span> 
            <span>C</span>
            <span>U</span>
            <span>L</span>
            <span>I</span>
            <span>A</span>
            <span>R </span> 
        </span>
    )
}