
export default function getSMTextStats(stat){
    if(stat === "Prowess") return "Strength and athletic ability"
    if(stat === "Effusion") return "How easily spells flow out of you. Your 'Mana'"
    if(stat === "Cantillation") return "Your ability to recite chants and religious rituals"
    if(stat === "Understanding") return "Your ability to learn technical skills and arcane magics"
    if(stat === "Licentiousness") return "Your ability to understand heretical and deviant magic"
    if(stat === "Insight") return "Your ability to perceive and understand esotericism"
    if(stat === "Agility") return "Your grace, speed, and dexterity"
    if(stat === "Resilience") return "Your robustness, stamina, and constitution"
}

