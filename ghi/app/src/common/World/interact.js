import { getItem } from "../Items/items";

export default function interact(interactAction, player, tile) {
  const actionArray = interactAction.split(" ");
  switch (interactAction) {
    default:
      console.log("Error: Interact Action not found");
      break;
    case "pick flower":
      return pickPlant(actionArray[1], player, tile);
  }
}

function pickPlant(plant, player, tile) {
  if (tile.amount > 0) {
    const plantObj = getItem(`ing ${plant}`);
    if (player.items.length === 0) {
        console.log("entered no items")
        player.items.push(plantObj)
        return `You picked a ${plant}`;
    } else {
      const playerItem = player.items.find((item) => {
        return item.name === plant;
      });

      if (playerItem === undefined) {
        player.items.push(plantObj);
      } else {
        playerItem.amount += 1;
      }
      tile.amount -= 1;
      return `You picked a ${plant}`;
    }
  } else {
    return `There are no more ${plant}s to pick`;
  }
}
