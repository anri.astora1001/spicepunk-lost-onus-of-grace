function randArr(length) {
  return Math.floor(Math.random() * length);
}

// String.prototype.capitalize = function(){
//     let str = this
//     const temp = str
//     str.charAt(0).toUpperCase() + temp.slice(1)
// }

function capitalize(str) {
  const temp = str;
  return str.charAt(0).toUpperCase() + temp.slice(1);
}

function cycleArr(arr, i) {
  if (i + 1 === arr.length) return 0;
  return i + 1;
}

function d20(){
  return Math.floor(Math.random() * 20 + 1)
}

export { randArr, capitalize, cycleArr , d20};
