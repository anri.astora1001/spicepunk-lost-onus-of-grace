export default function MainButton(props){

    return (
        <div
        className="col bg-secondary p-0 d-flex align-items-center justify-content-center cursor-pointer"
        style={{
            border:"1px solid black",
        }}
        >   {(props.children === undefined || props.children.length === 0) && <div className="bgc-accent btn-c"></div>}
            {props.children}
        </div>
    )
}