
export default function handleFormChange(event, setForm , form){
    const val = event.target.value
    const key = event.target.name

    let oldForm = form

    oldForm[key] = val

    setForm(form => ({
        ...oldForm,
    }))

    return oldForm
}