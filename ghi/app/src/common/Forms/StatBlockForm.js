import { useCallback, useState } from "react"
import LargeModal from "../Modals/LargeModal"
import SmallModal from "../Modals/SmallModal"
import getSMTextStats from "../stat-descriptions"

export default function StatBlockForm(){
    const [points, setPoints] = useState(10)

    const [prowess, setProwess] = useState(8)
    const [effusion, setEffusion] = useState(8)
    const [cantillation, setCantillation] = useState(8)
    const [understanding, setUnderstanding] = useState(8)
    const [licentiousness, setLicetiousness] = useState(8)
    const [insight, setInsight] = useState(8)
    const [agility, setAgility] = useState(8)
    const [resilience, setResilience] = useState(8)

    return (
        <div className="mb-3 w-50">
                <div className="row">
                    <div className="col">
                        <label
                        className="form-label ms-1"
                        >
                            Stats
                        </label>
                    </div>
                    <div className="col">
                        <span>Points: {points} </span>
                    </div>
                </div>
                <Field type={"top"} name={"Prowess"} sendData={setProwess} stat={prowess} sendPoints={setPoints} points={points} />
                <Field type={"alt"} name={"Effusion"} sendData={setEffusion} stat={effusion} sendPoints={setPoints} points={points} />
                <Field type={""} name={"Cantillation"} sendData={setCantillation} stat={cantillation} sendPoints={setPoints} points={points} />
                <Field type={"alt"} name={"Understanding"} sendData={setUnderstanding} stat={understanding} sendPoints={setPoints} points={points} />
                <Field type={""} name={"Licentiousness"} sendData={setLicetiousness} stat={licentiousness} sendPoints={setPoints} points={points} />
                <Field type={"alt"} name={"Insight"} sendData={setInsight} stat={insight} sendPoints={setPoints} points={points} />
                <Field type={""} name={"Agility"} sendData={setAgility} stat={agility} sendPoints={setPoints} points={points} />
                <Field type={"bottom alt"} name={"Resilience"} sendData={setResilience} stat={resilience} sendPoints={setPoints} points={points} />
            </div>
    )
}

function Field(props){
    const stat = props.stat
    const points = props.points
    const sendData = props.sendData
    const sendPoints = props.sendPoints

    const [displayModal, setDisplayModal] = useState(false)
    const [position, setPosition] = useState({})

    const [displayLargeModal, setDisplayLargeModal] = useState(false)


    const handleMouseEnter = useCallback((e)=>{
        const x_pos = e.target.offsetLeft
        const y_pos = e.target.offsetTop 
        setDisplayModal(true)
        if(JSON.stringify(position) === '{}'){
            setPosition(
                {
                    x: x_pos,
                    y: y_pos
                }
            )
        }
        
    },[position])
    const handleMouseLeave = useCallback(() =>{
        setDisplayModal(false)
        setPosition({})
    },[])

    const handleClick = () =>{
        console.log("entered")
        if(displayLargeModal){
            setDisplayLargeModal(false)
        } else {
            setDisplayLargeModal(true)
        }
    }


    let styling = ""
    if(props.type?.includes("bottom")){ 
        styling +="rounded-bottom " 
    }else {
        styling += "border-bottom-c-accent "
        if(props.type?.includes("top")){styling += "rounded-top "};
    }
    if(props.type?.includes("alt")){
        styling += "bgc-dark3 "
    } else {
        styling += "bgc-dark "
    }
 
    function increment() {
        if (points !== 0) {

            sendData(stat + 1)
            sendPoints(points - 1)
        }
    }

    const decrement = ()=>{
        if(stat !== 8){
            sendData(stat - 1)
            sendPoints(points + 1)
        }
    }

    return (
        <div className={`row ${styling}`}>
            <div className="col-md-6 my-auto cursor-pointer" >
                <span onMouseEnter={handleMouseEnter} 
                onMouseLeave={handleMouseLeave} 
                onClick={handleClick}
                className="fs-5 text-accent-reactive rel"> 
                    {props.name} 
                    <SmallModal position={position}  
                        display={displayModal} 
                        text={getSMTextStats(props.name)} /> 
                </span> 
                <LargeModal type={["stat",`${props.name}`]}  display={displayLargeModal} displayToggle={setDisplayLargeModal} text={'test'} /> 
            </div>
            <div className="col-sm-2 p-0 ms-auto">
                <input
                type="number"
                min="8"
                max="18"
                name="effusion"
                value={props.stat}
                className="bg-transparent text-accent border-0 w-100 my-1"
                />
            </div>
            <div onClick={increment} className="col-sm-1 border p-0 px-auto text-center bgc-accent text-c-dark rounded-start cursor-pointer">+</div>
            <div onClick={decrement} className="col-sm-1 border p-0 px-auto text-center bgc-accent text-c-dark rounded-end cursor-pointer">-</div>
        </div>
    )
}