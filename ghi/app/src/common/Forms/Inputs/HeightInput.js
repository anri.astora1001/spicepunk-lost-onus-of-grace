export default function HeightInput(props){
    return (
        <div
        className="mb-3"
        >
            <label 
            htmlFor="playerName" 
            className="form-label ms-1"
            >
                Height
            </label>
            <input 
            type="number" 
            name="height"
            min="120"
            max="220"
            className="form-control w-25 shadow" 
            id="playerHeight" 
            />
            <div 
            id="heightHelp" 
            className="form-text text-accent ms-1">
                How tall you are from 120-220 cm
            </div>
        </div>
    )
}