import { useState } from "react";

export default function LooksInput(props) {
  const [looks, setLooks] = useState(50);

  const handleChange = (e) => {
    const value = e.target.value;
    setLooks(value);
  };

  return (
    <div className="meb-3 mt-4">
      <label className="form-label ms-1">Looks: {looks}</label>
      <input
        onChange={handleChange}
        name="looks"
        value={looks}
        type="range"
        class="form-range"
        min="0"
        max="100"
      />
      <div className="form-text text-accent ms-1">
        Your Looks from Masc to Femme
      </div>
    </div>
  );
}
