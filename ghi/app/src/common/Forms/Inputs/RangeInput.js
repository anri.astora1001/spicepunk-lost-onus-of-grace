import { useState } from "react";
import { capitalize } from "../../util";

export default function RangeInput(props) {
  const title = capitalize(props.name);
  const [val, setVal] = useState(Math.floor((props.range[0] + props.range[1])/2));

  const handleChange = (e) => {
    const value = e.target.value;
    setVal(value);
  };

  return (
    <div className="meb-3 ms-4  border-start mt-3 ps-2">
      <label className="form-label ms-1">
        {title}: {val}
      </label>
      <input
        onChange={handleChange}
        name={`${props.name}`}
        value={val}
        type="range"
        class="form-range"
        min={`${props.range[0]}`}
        max={`${props.range[1]}`}
        step={`${props.range[2]}`}
      />
      <div className="form-text text-accent ms-1">{props.children}</div>
    </div>
  );
}

