
export default function RaceInput(props){
    return (
        <div className="mb-3">
            <label
            htmlFor="race"
            className="form-label ms-1"
            >
                Race
            </label>
            <select
            id="pronouns"
            name="race"
            className="form-select w-50 text-center shadow rounded-0 rounded-top" 
            >
                <option className="form-option" value="">Choose...</option>
                <option className="form-option">Human</option>
                <option className="form-option">Elf</option>
                <option className="form-option">Orc</option>
                <option className="form-option">Janwari</option>
            </select>
            <div 
            className="form-text text-accent ms-1">
                Your Race Options
            </div>
        </div>
    )
}