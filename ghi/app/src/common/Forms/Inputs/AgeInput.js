export default function AgeInput(props){

    return (
        <div
            className="mb-3"
            >
            <label 
            htmlFor="playerAge" 
            className="form-label ms-1"
            >
                Age
            </label>
            <input 
            type="number" 
            name="age"
            min="18"
            max="100"
            className="form-control w-25 shadow" 
            id="playerAge" 
            />
            <div 
            id="nameHelp" 
            className="form-text text-accent ms-1">
                How old you are
            </div>
        </div>
    )
}