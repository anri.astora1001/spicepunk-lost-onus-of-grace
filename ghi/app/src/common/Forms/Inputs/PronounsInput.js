export default function PronounsInput(props){
    return (
        <div className="mb-3">
            <label
            htmlFor="pronouns"
            className="form-label ms-1"
            >
                Pronouns
            </label>
            <select
            id="pronouns"
            name="pronouns"
            className="form-select w-50 shadow rounded-0 rounded-top text-center"
            >
                <option value="">Choose...</option>
                <option>She/Her/Hers</option>
                <option>He/Him/His</option>
                <option>They/Them/Theirs</option>
            </select>
            <div 
            id="nameHelp" 
            className="form-text text-accent ms-1">
                what others will refer to you as
            </div>
        </div>
    )
}