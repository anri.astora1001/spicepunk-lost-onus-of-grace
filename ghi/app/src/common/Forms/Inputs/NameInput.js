export default function NameInput(props){

    return (
        <div className="mb-3">
            <label 
            htmlFor="playerName" 
            className="form-label ms-1"
            >
                Name
            </label>
            <input 
            type="text" 
            name="name"
            className="form-control shadow" 
            id="playerName" 
            />
            <div 
            id="nameHelp" 
            className="form-text text-accent ms-1">
                This is your given name
            </div>
        </div>
    )
}