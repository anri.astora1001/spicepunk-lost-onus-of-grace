export default function BodyTypeInput(props){
    return (
        <div className="mb-3">
            <label
            htmlFor="body-type"
            className="form-label ms-1"
            >
                Body Type
            </label>
            <select
            name="body-type"
            className="form-select w-25 text-center shadow" 
            >
                <option className="form-option" value="">Choose...</option>
                <option className="form-option rounded">Type A</option>
                <option className="form-option rounded">Type B</option>
                <option className="form-option rounded">Type AB</option>
                <option className="form-option rounded">Type O</option>
            </select>
            <div 
            className="form-text text-accent ms-1">
                Affects starting race options.
            </div>
        </div>
    )
}