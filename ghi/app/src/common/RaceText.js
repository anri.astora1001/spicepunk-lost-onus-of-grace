import { useState } from "react";
import SmallModal from "./Modals/SmallModal";
import LargeModal from "./Modals/LargeModal";

export default function RaceText(props) {
  const race = props.race;
  const [showSmall, setShowSmall] = useState(false);
  const [showLarge, setShowLarge] = useState(false);
  const smallModalText = getRaceModalText(race);

  const handleMouseEnter = () => {
    setShowSmall(true);
  };
  const handleMouseLeave = () => {
    setShowSmall(false);
  };

  const handleToggle = () => {
    setShowLarge(!showLarge);
  };

  if ((race === undefined) | (race === "")) return;
  return [
    <span
      className={`${race}-text rel`}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={handleToggle}
    >
      {" "}
      {race}
      {!showLarge && <SmallModal text={smallModalText} display={showSmall} />}
    </span>,
    <LargeModal
      type={["race", `${race}`]}
      display={showLarge}
      displayToggle={setShowLarge}
    />,
  ];
}

function getRaceModalText(race) {
  if (race === "Human") return "a average race";
  if (race === "Janwari") return "a bestial race";
  return "undefined";
}
