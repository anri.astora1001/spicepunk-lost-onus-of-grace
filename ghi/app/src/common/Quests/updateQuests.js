export default function updateQuests(quests, player, check = "all"){
    console.log(quests)
    if(check === "all"){

    } else {
        if(check === "items"){
            const itemQuests = quests.filter((quest) =>{
                return quest.type === "item"
            })
            if(itemQuests){
                itemQuests.forEach(quest => {
                    updateQuest(quest.QuestId, player, quests)
                });
            }
        }
    }
}

function updateQuest(QuestId, player, quests){
    console.log(quests)
    switch (QuestId){
        default: console.log("ERROR: QuestId not found"); break;
        case "test0":
            updateTest0(player, quests)
    }
}

function updateTest0(player, quests){
    const flowerObj = player.items.find((item) =>{
        return item.name === "flower"
    })
    if(flowerObj === undefined) return

    console.log(quests)

    const QuestObj = quests.find((quest) =>{
        return quest.QuestId === "test0"
    })
    QuestObj.progress = `${flowerObj.amount}/5`
}