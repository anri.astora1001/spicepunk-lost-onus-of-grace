import { d20 } from "./util";

function jumpCheck(diff){
    const DC = diff * 5 + 15
    let roll = 0
    for(let i = 0; i < 5; i++) {
        roll += d20()
    }
    console.log("jumpRoll: ", roll)
    if (DC < roll){
        return "success"
    } else {
        return "fail"
    }
}

function climbCheck(height){
    const diff = height - 2
    const DC = diff * 5 + 15
    let roll = 0
    for(let i = 0; i < 5; i++) {
        roll += d20()
    }
    console.log("climbRoll: ", roll)
    if (DC < roll){
        return "success"
    } else {
        return Math.floor(Math.random() * height)
    }
}

export {jumpCheck, climbCheck}