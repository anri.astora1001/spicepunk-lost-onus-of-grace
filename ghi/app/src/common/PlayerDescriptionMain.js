import RaceText from "./RaceText";
import { randArr } from "./util";

export default function PlayerDescriptionMain(props) {
  const playerDict = props.form;
  console.log(playerDict);
  return (
    <div>
      <NameDrop name={playerDict.name}>, an adventurer of no renown, </NameDrop>
      <AgeAdj age={playerDict.age} />
      <RaceText race={playerDict.race} />
      <GenderNoun pronouns={playerDict.pronouns}> of</GenderNoun>
      <HeightAdj height={playerDict.height}>stature</HeightAdj>
    </div>
  );
}

function NameDrop(props) {
  if (!props.name) return;
  return <span>{props.name}{props.children}</span>;
}

function AgeAdj(props) {
  if (!props.age) return;
  return <span>{adjFromAge(props.age)}</span>;
}

function GenderNoun(props) {
  if (!props.pronouns) return;
  return <span>{genderFromNoun(props.pronouns)} {props.children} </span>;
}

function HeightAdj(props){
  if(!props.height) return
  return <span> {adjHeight(props.height)} {props.children}</span>
}

function genderFromNoun(noun) {
  console.log(noun);
  if (noun === "She") return "woman";
  if (noun === "He") return "man";
  if (noun === "They") return "person";
}

function adjHeight(height) {
  if (height < 140) {
    const tiny = ["tiny"];
    return tiny[randArr(tiny.length)];
  }
  if (height < 150) {
    const small = ["small"];
    return small[randArr(small.length)];
  }
  if (height < 160) {
    const very_short = ["very short", "petite"];
    return very_short[randArr(very_short.length)];
  }
  if (height < 170) {
    const short = ["short"];
    return short[randArr(short.length)];
  }
  if (height < 175) {
    const average = ["average"];
    return average[randArr(average.length)];
  }
  if (height < 180) {
    const above = ["above-average"];
    return above[randArr(above.length)];
  }
  if (height < 185) {
    const taller = ["taller"];
    return taller[randArr(taller.length)];
  }
  if (height < 190) {
    const tall = ["tall"];
    return tall[randArr(tall.length)];
  }
  if (height < 195) {
    const very_tall = ["very tall"];
    return very_tall[randArr(very_tall.length)];
  }
  if (height < 200) {
    const huge = ["huge"];
    return huge[randArr(huge.length)];
  }
  if (height < 210) {
    const herculean = ["herculean"];
    return herculean[randArr(herculean.length)];
  }
  if (height < 220) {
    const goliath = ["goliath"];
    return goliath[randArr(goliath.length)];
  }
  const colossus = ["colossus"];
  return colossus[randArr(colossus.length)];
}

function adjFromAge(age) {
  if (age < 20) {
    const young = ["young", "adolescent", "college-aged", "green", "teenaged"];
    return young[randArr(young.length)];
  }
  if (age < 26) {
    const young = ["young", "college-aged", "prime-adult"];
    return young[randArr(young.length)];
  }
  if (age < 35) {
    const adult = ["adult"];
    return adult[randArr(adult.length)];
  }
  if (age < 40) {
    const late_thirties = ["adult"];
    return late_thirties[randArr(late_thirties.length)];
  }
  if (age < 50) {
    const forties = ["adult", "quadragenarian"];
    return forties[randArr(forties.length)];
  }
  if (age < 60) {
    const fifties = ["older", "middle-aged", "quinquagenarian"];
    return fifties[randArr(fifties.length)];
  }
  if (age < 70) {
    const sixties = ["older", "senior", "sexagenarian"];
    return sixties[randArr(sixties.length)];
  }
  if (age > 100) {
    const elden = ["elden", "centenarian"];
    return elden[randArr(elden.length)];
  }
  if (age > 89) {
    const nineties = ["elder", "nonagenarian"];
    return nineties[randArr(nineties.length)];
  }
  if (age > 79) {
    const eighties = ["elder", "octogenarian"];
    return eighties[randArr(eighties.length)];
  }
  const seventies = ["elder", "septuagenarian", "senior"];
  return seventies[randArr(seventies.length)];
}
