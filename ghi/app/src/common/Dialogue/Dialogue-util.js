import { randArr } from "../util";

function getGreeting() {
  const greetings = [
    "Hello there.",
    "Yes?",
    "What brings you?",
    "This better be quick",
  ];
  return greetings[randArr(greetings.length)];
}

function getPlayerGreeting() {
  const greetings = [
    "Hello there.",
    "Yes?",
    "What brings you?",
    "This better be quick",
  ];
  return greetings[randArr(greetings.length)];
}

export { getGreeting, getPlayerGreeting };
