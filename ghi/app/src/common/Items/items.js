import { getIngredient} from "./ingrediants"

function getItem(searchString){
    const searchArray = searchString.split(" ")
    const category = searchArray[0]
    const search = searchArray[1]
   
    if(category === "ing" || category === "ingredient"){
        const item = getIngredient(search)
        if(item === undefined){
            console.log("Error: ingredient not found")
        } else {
            return item
        }
    }
}

export {getItem}