function getIngredientList(){
    return ["flower"]
}

function getIngredient(name){
    const list = getIngredientList()
    if(!list.includes(name)){
        return undefined
    } else {
        if(name === "flower"){
            return {name:"flower",type:["ingredient"], amount:1}
        }
    }
}

export {getIngredientList, getIngredient}