import {useNavigate } from "react-router-dom";

export default function NewGameButton(){
    const navigate = useNavigate()


    const newGame = () => {
        console.log("entered function")
        navigate("/newgame/character-creation")
    }

    return [
        
        <div
        onClick={newGame}
        className="bgc-accent btn-c "
        >
            <p className="mx-auto">
                New Game
            </p>
        </div>
    ]

}