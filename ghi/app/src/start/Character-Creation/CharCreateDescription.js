import { useState, useEffect } from "react"
import PlayerDescriptionMain from "../../common/PlayerDescriptionMain"
export default function CharCreateDescription(props){
    
    useEffect(()=>{
    },[props.form])

    return (
        <div
        className="text-accent fixed-container h-100"
        >
            <PlayerDescriptionMain form={props.form}/>
        </div>
    )
}