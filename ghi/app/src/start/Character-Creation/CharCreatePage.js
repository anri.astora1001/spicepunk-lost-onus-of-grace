import CharCreateForm from "./CharCreateForm";
import { useState } from "react";
import PlayerDescriptionMain from "../../common/PlayerDescriptionMain";

export default function CharCreatePage() {
  const [playerForm, setPlayerForm] = useState({});

  return (
    <div className="h-100">
      <div className="row h-100">
        <div className="col bgc-dark2 h-100 pt-5">
          <div
            style={{
              width: "50vw",
              paddingRight: "15%",
              paddingLeft: "15%",
            }}
          >
            <CharCreateForm sendData={setPlayerForm} />
          </div>
        </div>
        <div className="col bgc-dark pt-5 px-3">
          <div className="text-accent fixed-container h-100">
            <PlayerDescriptionMain form={playerForm} />
          </div>
        </div>
      </div>
    </div>
  );
}
