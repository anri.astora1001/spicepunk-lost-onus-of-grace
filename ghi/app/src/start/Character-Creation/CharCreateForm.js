import { useState, useEffect } from "react";
import handleFormChange from "../../common/forms";
import StatBlockForm from "../../common/Forms/StatBlockForm";
import LooksInput from "../../common/Forms/Inputs/LooksInput";
import RaceInput from "../../common/Forms/Inputs/RaceInput";
import BodyTypeInput from "../../common/Forms/Inputs/BodyTypeInput";
import HeightInput from "../../common/Forms/Inputs/HeightInput";
import AgeInput from "../../common/Forms/Inputs/AgeInput";
import PronounsInput from "../../common/Forms/Inputs/PronounsInput";
import NameInput from "../../common/Forms/Inputs/NameInput";
import RangeInput from "../../common/Forms/Inputs/RangeInput";

export default function CharCreateForm(props) {
  const [form, setForm] = useState({
    name: "",
  });

  const handleChange = (e) => {
    const updatedForm = handleFormChange(e, setForm, form);
    props.sendData(updatedForm);
  };

  useEffect(() => {}, [form]);

  console.log(form);

  return (
    <form className="text-accent" onChange={handleChange}>
      <NameInput />
      <PronounsInput />
      <AgeInput />
      <RaceInput />
      {form.race && [<HeightInput />, <BodyTypeInput />]}
      {form["body-type"] === "Type A" && (
        <RangeInput name={"chest"} range={[0, 100, 1]}>
          Your Chest Size
        </RangeInput>
      )}
      {form["body-type"] === "Type B" && (
        <RangeInput name={"package"} range={[0, 100, 1]}>
          Your Package Size
        </RangeInput>
      )}
      {form["body-type"] === "Type AB" && [
        <RangeInput name={"chest"} range={[0, 100, 1]}>
          Your Chest Size
        </RangeInput>,
        <RangeInput name={"package"} range={[0, 100, 1]}>
          Your Package Size
        </RangeInput>,
      ]}
      <LooksInput />
      <StatBlockForm />
    </form>
  );
}
