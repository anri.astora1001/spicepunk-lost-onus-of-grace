import NewGameButton from "./NewGameButton";
import MainButton from "../common/MainButton";
import { Link } from "react-router-dom";

import GameText from "../text/GameText";

export default function StartMenu() {
  return (
    <div className="container-fluid text-center vh-100">
      <div className="row bgc-dark h-70 text-white">
        <GameText>I am a @^RT:Janwari@</GameText>
      </div>
      <div className="row h-10 bgc-dark3">
        <MainButton />
        <MainButton>
          <NewGameButton />
        </MainButton>
        <MainButton>
          <div className="bgc-accent btn-c ">
            <Link className="mx-auto" to="/test/dialogue">
              Dialogue Testing
            </Link>
          </div>
        </MainButton>
        <MainButton>
          <div className="bgc-accent btn-c ">
            <Link className="mx-auto" to="/test/inventory">
              Inventory Testing
            </Link>
          </div>
        </MainButton>
        <MainButton><div className="bgc-accent btn-c ">
            <Link className="mx-auto" to="/test/main">
              Main Screen Testing
            </Link>
          </div></MainButton>
        <MainButton />
      </div>
      <div className="row h-10 bgc-dark3">
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
      </div>
      <div className="row h-10 bgc-dark3">
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
        <MainButton />
      </div>
    </div>
  );
}
